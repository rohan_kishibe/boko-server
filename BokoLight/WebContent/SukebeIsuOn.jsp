<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="js/main.js"></script>
		<title>凹ランプリモコン</title>
	</head>
	<body bgcolor="#252525" onload="load()">
		<form name="mainForm" id="mainForm" action="SukebeIsu" method="get">
			<input type="hidden" name="swich" id ="swich" value="<%=request.getAttribute("swich") %>">
			<input type="hidden" name="color" id ="color" value="<%=request.getAttribute("color") %>">
			<input type="hidden" name="type" id ="type" value="<%=request.getAttribute("type") %>">
		</form>
		<img src="img/chidori_lab.png" alt="千鳥lab" style="position:absolute;top:0px;left:0px;">
		<img src="img/band/band_pink.png" alt="" id="band">
		<img src="img/isu/isu_pink.png" alt="" id="isu" height="900">
		<img src="img/button/iro_red.png" alt="レッド" onclick="red()" style="position:absolute;top:1206px;left:20px;"/>
		<img src="img/button/iro_green.png" alt="グリーン" onclick="green()" style="position:absolute;top:1206px;left:210px;"/>
		<img src="img/button/iro_purple.png" alt="パープル" onclick="purple()" style="position:absolute;top:1206px;left:400px;"/>
		<img src="img/button/iro_white.png" alt="ホワイト" onclick="white()" style="position:absolute;top:1206px;left:590px;"/>
		<img src="img/button/iro_yellow.png" alt="イエロー" onclick="yellow()" style="position:absolute;top:1368px;left:20px;"/>
		<img src="img/button/iro_blue.png" alt="ブルー" onclick="blue()" style="position:absolute;top:1368px;left:210px;"/>
		<img src="img/button/iro_pink.png" alt="ピンク" onclick="pink()" style="position:absolute;top:1368px;left:400px;"/>
		<img src="img/button/iro_rainbow.png" alt="レインボー" onclick="rainbow()" style="position:absolute;top:1368px;left:590px;"/>
		<img src="img/button/on.png" alt="電気ON" onclick="swich_off()" style="position:absolute;top:1191px;left:765px;"/>
	</body>
</html>