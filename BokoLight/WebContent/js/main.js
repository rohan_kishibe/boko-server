function swich_on() {
	document.getElementById("swich").value="on";
	document.mainForm.submit();
}

function swich_off() {
	document.getElementById("swich").value="off";
	document.mainForm.submit();
}

function load(){
	var color = document.getElementById("color").value;
	var type = document.getElementById("type").value;
	if(color=='0'){
		document.getElementById("isu").src="img/isu/isu_pink.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_pink_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_pink_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_pink_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_pink_4.png";
		}
	}
	if(color=='1'){
		document.getElementById("isu").src="img/isu/isu_red.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_red_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_red_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_red_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_red_4.png";
		}
	}
	if(color=='2'){
		document.getElementById("isu").src="img/isu/isu_green.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_green_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_green_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_green_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_green_4.png";
		}
	}
	if(color=='3'){
		document.getElementById("isu").src="img/isu/isu_purple.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_purple_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_purple_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_purple_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_purple_4.png";
		}
	}
	if(color=='4'){
		document.getElementById("isu").src="img/isu/isu_white.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_white_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_white_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_white_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_white_4.png";
		}
	}
	if(color=='5'){
		document.getElementById("isu").src="img/isu/isu_yellow.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_yellow_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_yellow_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_yellow_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_yellow_4.png";
		}
	}
	if(color=='6'){
		document.getElementById("isu").src="img/isu/isu_blue.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_blue_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_blue_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_blue_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_blue_4.png";
		}
	}
	if(color=='7'){
		document.getElementById("isu").src="img/isu/isu_rainbow.png";
		if(type=='0') {
			document.getElementById("band").src="img/band/band_rainbow_1.png";
		} else if(type=='1') {
			document.getElementById("band").src="img/band/band_rainbow_2.png";
		} else if(type=='2') {
			document.getElementById("band").src="img/band/band_rainbow_3.png";
		} else if(type=='3') {
			document.getElementById("band").src="img/band/band_rainbow_4.png";
		}
	}
}

function red(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "1") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="1";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function green(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "2") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="2";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function purple(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "3") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="3";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function white(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "4") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="4";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function yellow(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "5") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="5";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function blue(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "6") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="6";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function pink(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "0") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="0";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}

function rainbow(){
	document.getElementById("swich").value="on";
	var type = parseInt(document.getElementById("type").value);
	if(document.getElementById("color").value === "7") {
		type = type + 1;
	} else {
		type = 0;
	}
	document.getElementById("color").value="7";
	document.getElementById("type").value=type;
	document.mainForm.submit();
}
