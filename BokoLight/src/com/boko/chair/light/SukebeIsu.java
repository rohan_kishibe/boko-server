package com.boko.chair.light;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SukebeIsu
 */
@WebServlet(name = "SukebeIsu", urlPatterns = { "/SukebeIsu" })
public class SukebeIsu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ON = "on";
	private static final String OFF = "off";

	/**
     * @see HttpServlet#HttpServlet()
     */
    public SukebeIsu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 呼び出し元Jspからデータ受け取り
		request.setCharacterEncoding("UTF8");
		String swich = "off";
		String color = "0";
		String type = "0";
		// 画面から値取得
		if (null != request.getParameter("swich") && !"".equals(request.getParameter("swich"))) {
			swich = request.getParameter("swich");
		}
		if (null != request.getParameter("color") && !"".equals(request.getParameter("color"))) {
			color = request.getParameter("color");
		}
		if (null != request.getParameter("type") && !"".equals(request.getParameter("type"))) {
			type = request.getParameter("type");
		}

		// 画面に返す値
		request.setAttribute("swich",swich);
		request.setAttribute("color",color);
		request.setAttribute("type",type);

		Properties prop = new Properties();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("ctrl.properties")) {
			prop.load(is);
			// 種類が最大値の時に初期値に戻す
			if(type.equals(prop.getProperty("ptnNum"))){
				type = "0";
				request.setAttribute("type",type);
			}
			// pythonのファイル格納場所を取得
			String pyFileName = prop.getProperty(color + "_" + type);
			System.out.println(color + "_" + type);
			System.out.println(pyFileName);
			// 表示する画面の選択
			if (ON.equals(swich)) {
				execSh(pyFileName);
				getServletConfig().getServletContext().getRequestDispatcher("/SukebeIsuOn.jsp").forward(request, response);
			} else if (OFF.equals(swich)) {
				// ネットワークインターフェイスを取得
				Enumeration<NetworkInterface> testMIP = NetworkInterface.getNetworkInterfaces();
				StringBuilder ipadd = new StringBuilder();
				if (null != testMIP) {
					while (testMIP.hasMoreElements()) {
						// ネットワークインターフェイスを１件取得
						NetworkInterface testNI = testMIP.nextElement();
						System.out.println(testNI.getDisplayName());
						if ("wlan0".equals(testNI.getDisplayName())) {
							// ネットワークインターフェイスからInetAddressを取得
							Enumeration<InetAddress> testInA = testNI.getInetAddresses();
							while (testInA.hasMoreElements()) {
								// InetAddressを１件取得
								InetAddress testIP = testInA.nextElement();
								ipadd.append(testIP.getHostAddress());
								ipadd.append(";");
							}
						}
					}
				}
				request.setAttribute("addr",ipadd.toString());
				pyFileName = prop.getProperty(OFF);
				execSh(pyFileName);
				getServletConfig().getServletContext().getRequestDispatcher("/SukebeIsuOff.jsp").forward(request, response);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}

		return;
	}

	/**
	 * パイソン実行
	 * @param fileName 実行するパイソンファイル名
	 * @throws Exception
	 */
	private void execSh(String fileName) throws Exception {
		Process process = new ProcessBuilder("sudo", "python", fileName).start();
		String text;
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is, "UTF-8");
		BufferedReader reader = new BufferedReader(isr);
		StringBuilder builder = new StringBuilder();
		int c;
		while ((c = reader.read()) != -1) {
			builder.append((char) c);
		}
		InputStream es = process.getErrorStream();
		InputStreamReader esr = new InputStreamReader(es, "UTF-8");
		BufferedReader ereader = new BufferedReader(esr);
		int ce;
		while ((ce = ereader.read()) != -1) {
			builder.append((char) ce);
		}
		// 実行結果を格納
		text = builder.toString();
		int ret = process.waitFor();

		System.out.println(text);
		System.out.println(ret);
	}
}
