# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import RPi.GPIO as GPIO
import time
import os

from neopixel import *

# LED strip configuration:
LED_1_COUNT      = 60      # Number of LED pixels.
LED_1_PIN        = 18      # GPIO pin connected to the pixels (must support PWM! GPIO 13 and 18 on RPi 3).
LED_1_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_1_DMA        = 5       # DMA channel to use for generating signal (Between 1 and 14)
LED_1_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_1_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_1_CHANNEL    = 0       # 0 or 1
LED_1_STRIP      = ws.WS2811_STRIP_GRB	

LED_2_COUNT      = 60      # Number of LED pixels.
LED_2_PIN        = 13      # GPIO pin connected to the pixels (must support PWM! GPIO 13 or 18 on RPi 3).
LED_2_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_2_DMA        = 5      # DMA channel to use for generating signal (Between 1 and 14)
LED_2_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_2_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_2_CHANNEL    = 1       # 0 or 1
LED_2_STRIP      = ws.WS2811_STRIP_GRB	

def colorWipe(color, wait_ms=50):
	global strip1
	global strip2
        """Wipe color across display a pixel at a time."""
        for i in range(strip1.numPixels()):
                strip1.setPixelColor(i, color)
                strip2.setPixelColor(i, color)
                strip1.show()
                strip2.show()
                time.sleep(wait_ms/1000.0)

def theaterChase(color, wait_ms=50, iterations=10):
	global strip1
	global strip2
        """Movie theater light style chaser animation."""
        for j in range(iterations):
                for q in range(3):
                        for i in range(0, strip1.numPixels(), 3):
                                strip1.setPixelColor(i+q, color)
                                strip2.setPixelColor(i+q, color)
                        strip1.show()
                        strip2.show()
                        time.sleep(wait_ms/1000.0)
                        for i in range(0, strip1.numPixels(), 3):
                                strip1.setPixelColor(i+q, 0)
                                strip2.setPixelColor(i+q, 0)

def wheel(pos):
        """Generate rainbow colors across 0-255 positions."""
        if pos < 85:
                return Color(pos * 3, 255 - pos * 3, 0)
        elif pos < 170:
                pos -= 85
                return Color(255 - pos * 3, 0, pos * 3)
        else:
                pos -= 170
                return Color(0, pos * 3, 255 - pos * 3)

def rainbow(wait_ms=20, iterations=1):
        """Draw rainbow that fades across all pixels at once."""
        for j in range(256*iterations):
                for i in range(strip1.numPixels()):
                        strip1.setPixelColor(i, wheel((i+j) & 255))
                        strip2.setPixelColor(i, wheel((i+j) & 255))
                strip1.show()
                strip2.show()
                time.sleep(wait_ms/1000.0)

def rainbowCycle(wait_ms=20, iterations=5):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        for j in range(256*iterations):
                for i in range(strip1.numPixels()):
                        strip1.setPixelColor(i, wheel((int(i * 256 / strip1.numPixels()) + j) & 255))
                        strip2.setPixelColor(i, wheel((int(i * 256 / strip2.numPixels()) + j) & 255))
                strip1.show()
                strip2.show()
                time.sleep(wait_ms/1000.0)

def theaterChaseRainbow(wait_ms=50):
        """Rainbow movie theater light style chaser animation."""
        for j in range(256):
                for q in range(3):
                        for i in range(0, strip1.numPixels(), 3):
                                strip1.setPixelColor(i+q, wheel((i+j) % 255))
                                strip2.setPixelColor(i+q, wheel((i+j) % 255))
                        strip1.show()
                        strip2.show()
                        time.sleep(wait_ms/1000.0)
                        for i in range(0, strip1.numPixels(), 3):
                                strip1.setPixelColor(i+q, 0)
                                strip2.setPixelColor(i+q, 0)

# Main program logic follows:
if __name__ == '__main__':

	GPIO.setmode(GPIO.BCM)
	GPIO.setup(10, GPIO.OUT)
	GPIO.output(10, 1)
	time.sleep(0.05)
	GPIO.output(10, 0)
	GPIO.cleanup()

        # kill rainbow process
        os.system('pkill -f rainbow1.py')
        os.system('pkill -f rainbow2.py')

        pid = os.fork()
        if pid == 0:
		# Create NeoPixel objects with appropriate configuration for each strip.
		strip1 = Adafruit_NeoPixel(LED_1_COUNT, LED_1_PIN, LED_1_FREQ_HZ, LED_1_DMA, LED_1_INVERT, LED_1_BRIGHTNESS, LED_1_CHANNEL, LED_1_STRIP)
		strip2 = Adafruit_NeoPixel(LED_2_COUNT, LED_2_PIN, LED_2_FREQ_HZ, LED_2_DMA, LED_2_INVERT, LED_2_BRIGHTNESS, LED_2_CHANNEL, LED_2_STRIP)

		# Intialize the library (must be called once before other functions).
		strip1.begin()
		strip2.begin()

		while True:
        		rainbow()
        		rainbow()
        		rainbow()
			rainbowCycle()
			rainbowCycle()
