#!/bin/bash

# factory reset script
# /srv/admin/facroty_reset.sh

echo "(1/5) stop tomcat..."
systemctl stop tomcat7

echo "(2/5) clear old application..."
rm -rfv /var/lib/tomcat7/webapps/*

echo "(3/5) clear tomcat caches..."
rm -rfv /var/cache/tomcat7/*

echo "(4/5) ensure newest application..."
rm -rfv /srv/boko-server/
cp -rpv /srv/default/boko-server/ /srv/boko-server/
cp -rv /srv/boko-server/html/ROOT/ /var/lib/tomcat7/webapps/
cp /srv/boko-server/BokoLight/release/BokoLight.war /var/lib/tomcat7/webapps/BokoLight.war

echo "(5/5) start tomcat..."
systemctl start tomcat7

exit 0

