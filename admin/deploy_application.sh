#!/bin/bash

# deploy application script
# /srv/admin/deploy_application.sh

echo "(1/7) download newest application..."
rm -rfv /srv/boko-server-newest/
git clone https://bitbucket.org/rohan_kishibe/boko-server.git /srv/boko-server-newest
# TODO:download check

echo "(2/7) stop tomcat..."
systemctl stop tomcat7

echo "(3/7) clear old application..."
rm -rfv /var/lib/tomcat7/webapps/*

echo "(4/7) clear tomcat caches..."
rm -rfv /var/cache/tomcat7/*

echo "(5/7) ensure newest application..."
rm -rfv /srv/boko-server/
cp -rpv /srv/boko-server-newest/ /srv/boko-server/
cp -rv /srv/boko-server/html/ROOT/ /var/lib/tomcat7/webapps/
cp /srv/boko-server/BokoLight/release/BokoLight.war /var/lib/tomcat7/webapps/BokoLight.war

echo "(6/7) start tomcat..."
systemctl start tomcat7

echo "(7/7) custom script..."
sh /srv/boko-server/admin/custom_script.sh

exit 0

